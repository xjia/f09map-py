# -*- coding: utf-8 -*-

import base64
import datetime
import jinja2
import json
import logging
import os
import urllib
import webapp2

from google.appengine.api import images
from google.appengine.api import memcache
from google.appengine.ext import db
from google.appengine.ext import ndb

class Photo(ndb.Model):
  data = ndb.BlobProperty(required=True)
  width = ndb.IntegerProperty(indexed=False, required=True)
  height = ndb.IntegerProperty(indexed=False, required=True)
  format = ndb.IntegerProperty(indexed=False, required=True)

class Marker(ndb.Model):
  x = ndb.IntegerProperty(required=True)
  y = ndb.IntegerProperty(required=True)

class Event(ndb.Model):
  content = ndb.TextProperty(required=True)
  photos = ndb.KeyProperty(kind=Photo, repeated=True)
  marker = ndb.KeyProperty(kind=Marker, required=True)
  ip_address = ndb.StringProperty(indexed=False, required=True)
  create_time = ndb.DateTimeProperty(auto_now_add=True)

jinja_environment = jinja2.Environment(
  loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class MainHandler(webapp2.RequestHandler):
  def get(self):
    template_values = {}
    template = jinja_environment.get_template('index.html')
    self.response.out.write(template.render(template_values))

# N.B. Without blobstore we cannot limit file size
class UploadHandler(webapp2.RequestHandler):
  def get_photo(self, name):
    max_width = 600
    max_height = 600
    data = self.request.get(name)

    if data:
      photo = Photo()
      image = images.Image(data)

      if image.width > max_width or image.height > max_height:
        image.resize(width=max_width, height=max_height)  # preserves aspect ratio
        data = image.execute_transforms(output_encoding=images.JPEG)
        photo.format = images.JPEG
      else:
        photo.format = image.format

      photo.data = db.Blob(data)
      photo.width = image.width
      photo.height = image.height      
      return photo.put()
    else:
      return None

  def post(self):
    self.response.headers['Content-Type'] = 'application/json'

    try:
      assert memcache.get(self.request.remote_addr) is None, '慢点发，捉毛急'
      memcache.add(key=self.request.remote_addr, value=True, time=5)  # 5 sec

      x = int(self.request.get('x') or 0)
      y = int(self.request.get('y') or 0)
      event_content = self.request.get('content0')

      logging.debug(self.request.get('content'))
      logging.debug(self.request.get('content0'))

      try:
        event_content = base64.b64decode(event_content)
        logging.debug('base64 = %s' % event_content)
      except:
        logging.debug('base64 failed')
        event_content = event_content

      try:
        event_content = unicode(event_content, 'utf-8')
        logging.debug('unicode = %s' % event_content)
      except:
        logging.debug('unicode failed')
        event_content = event_content

      assert x > 0 and x < 1000, 'X should be between 0 and 1000'
      assert y > 0 and y < 1000, 'Y should be between 0 and 1000'
      assert len(event_content) >= 2, '记忆描述太短'
      assert len(event_content) <= 300, '记忆描述太长'

      qry = Marker.query(ndb.AND(Marker.x == x, Marker.y == y))
      itr = qry.iter(keys_only=True)
      if itr.has_next():
        marker_key = itr.next()
        logging.debug("reuse marker")
      else:
        marker = Marker(x=x,y=y)
        marker_key = marker.put()
        memcache.delete('markers')
        logging.debug("new marker")

      photo_keys = []

      photo_key = self.get_photo('photo1')
      if photo_key:
        photo_keys.append(photo_key)

      photo_key = self.get_photo('photo2')
      if photo_key:
        photo_keys.append(photo_key)

      photo_key = self.get_photo('photo3')
      if photo_key:
        photo_keys.append(photo_key)

      logging.debug("photo_keys = %s" % [key.urlsafe() for key in photo_keys])

      event = Event(content = event_content,
                    photos = photo_keys,
                    marker = marker_key,
                    ip_address = self.request.remote_addr)

      event_key = event.put()
      if event_key:
        self.response.write(json.dumps({
          'status': 'success',
          'marker': marker_key.urlsafe(),
          'event_id': event_key.urlsafe()
          }))
      else:
        self.response.write(json.dumps({ 'status': 'failure', 'reason': '未知错误' }))

    except AssertionError, error:
      self.response.write(json.dumps({ 'status': 'failure', 'reason': "%s" % error }))

    except images.BadImageError, error:
      self.response.write(json.dumps({
        'status': 'failure',
        'reason': '照片数据已损坏 (%s)' % error
        }))
    except images.LargeImageError:
      self.response.write(json.dumps({
        'status': 'failure',
        'reason': '照片过大无法处理'
        }))
    except images.NotImageError, error:
      self.response.write(json.dumps({
        'status': 'failure',
        'reason': '无法识别照片格式 (%s)' % error
        }))
    except images.Error, error:
      self.response.write(json.dumps({ 'status': 'failure', 'reason': "%s" % error }))

def content_type(format):
  if format == images.JPEG:
    return 'image/jpeg'
  elif format == images.PNG:
    return 'image/png'
  elif format == images.WEBP:
    return 'image/webp' 
  elif format == images.BMP:
    return 'image/x-ms-bmp'
  elif format == images.GIF:
    return 'image/gif'
  elif format == images.ICO:
    return 'image/vnd.microsoft.icon'
  elif format == images.TIFF:
    return 'image/tiff'
  return None

class PhotoHandler(webapp2.RequestHandler):
  def get_photo(self, urlsafe_key):
    photo = memcache.get('photo:%s' % urlsafe_key)
    if photo is not None:
      return photo
    else:
      key = ndb.Key(urlsafe=urlsafe_key)
      photo = key.get()
      if not memcache.add('photo:%s' % urlsafe_key, photo):
        logging.error('Memcache add photo failed.')
      return photo

  def get(self, urlsafe_key):
    thirty_days_in_seconds = 4320000
    expires_date = datetime.datetime.now() + datetime.timedelta(days=30)
    HTTP_HEADER_FORMAT = '%a, %d %b %Y %H:%M:00 GMT'

    self.response.headers['Expires'] = expires_date.strftime(HTTP_HEADER_FORMAT)
    self.response.headers['Cache-Control'] = "public, max-age=%s" % thirty_days_in_seconds

    photo = self.get_photo(urlsafe_key)
    
    self.response.headers['Content-Type'] = content_type(photo.format)
    self.response.write(photo.data)

class MarkersHandler(webapp2.RequestHandler):
  def get_data(self):
    data = memcache.get('markers')
    if data is not None:
      return data
    else:
      data = []
      for marker in Marker.query():
        data.append({
          'id': marker.key.urlsafe(),
          'x': marker.x,
          'y': marker.y
          })
      if not memcache.add('markers', data):
        logging.error('Memcache add markers failed.')
      return data

  def get(self):
    data = self.get_data()
    self.response.headers['Content-Type'] = 'application/json'
    self.response.write(json.dumps(data))

class MarkerHandler(webapp2.RequestHandler):
  def get_page(self, urlsafe_key):
    page = memcache.get('marker_page:%s' % urlsafe_key)
    if page is not None:
      return page
    else:
      marker_key = ndb.Key(urlsafe=urlsafe_key)
      marker = marker_key.get()
      events = []
      for event in Event.query(Event.marker == marker_key).order(-Event.create_time):
        events.append({
          'id': event.key.urlsafe(),
          'content': event.content,
          'photos': [key.urlsafe() for key in event.photos],
          'ip_address': event.ip_address,
          'create_time': (event.create_time + datetime.timedelta(hours=8)).strftime('%Y-%m-%d %H:%M')
          })

      template_values = {
        'marker_x': marker.x,
        'marker_y': marker.y,
        'events': events
      }
      template = jinja_environment.get_template('events.html')
      page = template.render(template_values)
      if not memcache.add(key='marker_page:%s' % urlsafe_key, value=page, time=10):
        logging.error('Memcache add page failed.')
      return page

  def get(self, urlsafe_key):
    page = self.get_page(urlsafe_key)
    self.response.out.write(page)

class ExportHandler(webapp2.RequestHandler):
  def get(self):
    events = []
    for marker in Marker.query():
      for event in Event.query(Event.marker == marker.key):
        events.append({
          'x': marker.x,
          'y': marker.y,
          'content': event.content,
          'photos': [key.urlsafe() for key in event.photos],
          'ip_address': event.ip_address,
          'create_time': int(event.create_time.strftime("%s"))
          })
    self.response.headers['Content-Type'] = 'application/json'
    self.response.write(json.dumps(events))

app = webapp2.WSGIApplication([
  # ('/', MainHandler),
  # ('/upload', UploadHandler),
  ('/photo/(.+)', PhotoHandler),
  # ('/markers', MarkersHandler),
  # ('/marker/(.+)', MarkerHandler),
  ('/export', ExportHandler)
])
