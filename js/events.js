$(function(){

$('.permalink').tooltip({ placement: 'left' });

var linkify = function(text){
  var urlRegex = /(https?:\/\/([^\s]+))/g;
  return text.replace(urlRegex, '<a href="$1" target="_blank">$2</a>');
};

var shorten_limit = 36;

$('.media blockquote p.lead').each(function(i, p){
  var $p = $(p);
  $p.html(linkify($p.html()));
  $('a', $p).each(function(j, a){
    var $a = $(a);
    if ($a.html().length > shorten_limit) {
      $a.html($a.html().substr(0, shorten_limit) + "...");
    }
  });
});

var rotate_photo = function(deg){
  return function(event){
    var value = $(this).data('deg');
    if (value > 0 || value < 0) {
      value += deg;
    } else {
      value = deg;
    }
    $(this).data('deg', value);
    $(this).rotate({ animateTo: value });
    event.preventDefault();
    return false;
  };
};

$('.media img').rotate({
  bind: {
    click: rotate_photo(90),
    contextmenu: rotate_photo(-90)
  }
});

});
