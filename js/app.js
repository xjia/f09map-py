$(function(){

var precision = 1000;
var map_width = 5000;
var map_height = 2907;
var marker_width = 20;
var marker_height = 20;

var draw_marker = function(marker){
  var left = map_width * marker.x / 1000 - marker_width / 2;
  var top = map_height * marker.y / 1000 - marker_height / 2;
  $('#markers').append('<div class="marker-container">' +
                       '<a class="marker"' +
                       ' href="/marker/' + marker.id + '"' +
                       ' target="' + marker.id + '"' +
                       ' id="' + marker.id + '"' +
                       ' data-x="' + marker.x + '"' +
                       ' data-y="' + marker.y + '"' +
                       ' style="position: relative;' +
                       ' left: ' + left + 'px;' +
                       ' top: ' + top + 'px;"></a></div>');
};

var load_markers = function(callback){
  $.blockUI({ message: '<h3>正在加载标记……</h3>' });
  $.get('/markers', function(markers){
    var i;
    $('#markers').html('');
    for (i = 0; i < markers.length; i++) {
      draw_marker(markers[i]);
    }
    $('.marker').on('contextmenu', function(event){
      var x = $(this).data('x');
      var y = $(this).data('y');
      show_event_modal(x, y);
      event.preventDefault();
      return false;
    }).tooltip({ title: '左键点击查看记忆 右键添加新的记忆' });
    $.unblockUI();
    if (callback !== undefined) callback();
  }, 'json');
};

var show_event_modal = function(x, y){
  $('.new-event.modal').modal('show');
  $('#event-x').val(x);
  $('#event-y').val(y);
};

var $char_count = $('#char-count');

$('#map').on('contextmenu', function(event){
  var x = Math.round(precision * event.offsetX / map_width);
  var y = Math.round(precision * event.offsetY / map_height);
  show_event_modal(x, y);
  event.preventDefault();
  return false;
});

$('.new-event.modal').on('show', function(){
  $('input', $(this)).val('');
  $('textarea', $(this)).val('');
  $char_count.html('300');
  $('.submit').addClass('disabled');
});

$('.new-event.modal').on('hidden', function(){
  $('.new-event.modal .submit').removeClass('disabled');
  $.unblockUI();
});

$('#content').bind('input propertychange', function(){
  var remlen = 300 - $(this).val().length;
  $char_count.html('' + remlen);
  if (remlen <= 10) {
    $char_count.addClass('superwarn');
  } else {
    $char_count.removeClass('superwarn');
  }
  if (remlen < 0 || remlen >= 300) {
    $('.new-event.modal .submit').addClass('disabled');
  } else {
    $('.new-event.modal .submit').removeClass('disabled');
  }
});

$('.new-event.modal form').submit(function(){
  return false;
});

$('.new-event.modal .submit').click(function(){
  if ($(this).hasClass('disabled')) {
    return false;
  }
  $(this).addClass('disabled');
  $.blockUI({
    message: '<h3>正在保存……</h3>' +
    '<div class="progress progress-striped active">' +
    '<div class="bar">0%</div>' +
    '</div>'
  });
  var bar = $('.progress .bar');
  $('.new-event.modal form').ajaxSubmit({
    beforeSerialize: function($form, options){
      $('#content0').val(base64_encode($('#content').val()));
    },
    beforeSend: function(){
      var percentVal = '0%';
      bar.width(percentVal).html(percentVal);
    },
    uploadProgress: function(event, position, total, percentComplete){
      var percentVal = percentComplete + '%';
      bar.width(percentVal).html(percentVal);
    },
    dataType: 'json',
    success: function(data){
      if (data.status === 'success') {
        $('.new-event.modal').modal('hide');
        load_markers(function(){
          setTimeout(function(){
            $('#' + data.marker).popover({
              html: 'true',
              placement: 'top',
              trigger: 'manual',
              title: '<b>记忆已保存</b>',
              content: '<a href="/marker/' + data.marker + '#' + data.event_id + '" target="' + data.marker + '">点击这里查看</a>'
            }).popover('show');
            setTimeout(function(){
              $('#' + data.marker).popover('destroy');
            }, 5000);
          }, 500);
        });
      } else {
        $.unblockUI();
        setTimeout(function(){
          window.alert(data.reason);
          $('.new-event.modal .submit').removeClass('disabled');
        }, 500);
      }
    },
    error: function(){
      window.alert('未知错误');
      window.location.reload();
    }
  });
});

$('#viewer').scrollTop(1310).scrollLeft(1750).dragscrollable({
  dragSelector: '.drag:first',
  acceptPropagatedEvent: false
});

load_markers(function(){
  window.setTimeout(function(){ $('#hint').fadeOut(); }, 10000);
  $('#splash').hide();
});

});
