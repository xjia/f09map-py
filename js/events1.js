$(function(){

$('.permalink').tooltip({ placement: 'left' });

var linkify = function(text){
  var urlRegex = /(https?:\/\/([^\s]+))/g;
  return text.replace(urlRegex, '<a href="$1" target="_blank">$2</a>');
};

var shorten_limit = 36;

$('.media blockquote p.lead').each(function(i, p){
  var $p = $(p);
  $p.html(linkify($p.html()));
  $('a', $p).each(function(j, a){
    var $a = $(a);
    if ($a.html().length > shorten_limit) {
      $a.html($a.html().substr(0, shorten_limit) + "...");
    }
  });
});

var rotate_photo = function(deg){
  return function(event){
    var value = $(this).data('deg');
    if (value > 0 || value < 0) {
      value += deg;
    } else {
      value = deg;
    }
    $(this).data('deg', value);
    $(this).rotate({ animateTo: value });
    event.preventDefault();
    return false;
  };
};

$('.media img').rotate({
  bind: {
    click: rotate_photo(90),
    contextmenu: rotate_photo(-90)
  }
});

//===

var show_event_modal = function(x, y){
  $('.new-event.modal').modal('show');
  $('#event-x').val(x);
  $('#event-y').val(y);
};

var $char_count = $('#char-count');

$('#add-event').click(function(event){
  var x = $(this).data('x');
  var y = $(this).data('y');
  show_event_modal(x, y);
  event.preventDefault();
  return false;
});

$('.new-event.modal').on('show', function(){
  $('input', $(this)).val('');
  $('textarea', $(this)).val('');
  $char_count.html('300');
  $('.submit').addClass('disabled');
});

$('.new-event.modal').on('hidden', function(){
  $('.new-event.modal .submit').removeClass('disabled');
  $.unblockUI();
});

$('#content').bind('input propertychange', function(){
  var remlen = 300 - $(this).val().length;
  $char_count.html('' + remlen);
  if (remlen <= 10) {
    $char_count.addClass('superwarn');
  } else {
    $char_count.removeClass('superwarn');
  }
  if (remlen < 0 || remlen >= 300) {
    $('.new-event.modal .submit').addClass('disabled');
  } else {
    $('.new-event.modal .submit').removeClass('disabled');
  }
});

$('.new-event.modal form').submit(function(){
  return false;
});

$('.new-event.modal .submit').click(function(){
  if ($(this).hasClass('disabled')) {
    return false;
  }
  $(this).addClass('disabled');
  $.blockUI({
    message: '<h3>正在保存……</h3>' +
    '<div class="progress progress-striped active">' +
    '<div class="bar">0%</div>' +
    '</div>'
  });
  var bar = $('.progress .bar');
  $('.new-event.modal form').ajaxSubmit({
    beforeSerialize: function($form, options){
      $('#content0').val(base64_encode($('#content').val()));
    },
    beforeSend: function(){
      var percentVal = '0%';
      bar.width(percentVal).html(percentVal);
    },
    uploadProgress: function(event, position, total, percentComplete){
      var percentVal = percentComplete + '%';
      bar.width(percentVal).html(percentVal);
    },
    dataType: 'json',
    success: function(data){
      if (data.status === 'success') {
        window.location.reload();
      } else {
        $.unblockUI();
        setTimeout(function(){
          window.alert(data.reason);
          $('.new-event.modal .submit').removeClass('disabled');
        }, 500);
      }
    },
    error: function(){
      window.alert('未知错误');
      window.location.reload();
    }
  });
});

});
